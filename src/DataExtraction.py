import glob
import json
import os
import collections

k = "klee/"
v = "verifuzz/"
specific = "../evaluation/sv-benchmark/arrays/sanfoundary_24-1/"+ v
url = specific+"testcov/output/"
def data_extrac():
    """
    Extracts data from all info files in the `info_files` folder and from `results.json``
    as well and combine the result in the `output_tar/result_tar.info`
    """
    filenames = glob.glob(
        url+"/info_files/*.info"
    )
    columns = []
    for filename in filenames:
        getfileName = os.path.basename(str(filename))
        getFileNameWithOut = os.path.splitext(getfileName)[0]
        lines = []
        with open(
            url+"results.json"
        ) as json_file:
            data = json.load(json_file)
            for set in data:
                if getFileNameWithOut in set["Test"]:
                    lines.insert(0, set["Returncode"])
        for line in open(filename):
            stop = False
            if line.strip()[:3] == "DA:" and not stop:
                lines.append(line.strip("\n")[3:])
            elif "harness.c" in line.strip():
                stop = True
            if stop:
                break
        columns.append(lines)
    rows = zip(*columns)
    with open("../output_tar/merge.info", "w") as outfile:
        for row in rows:
            outfile.write("\t\t\t".join(map(str,row)))
            outfile.write("\n")


def make_data_struc():
    """
    Puts the data of `output_tar/result_tar.info` file into `OrderedDict` structure.
    The return type is `OrderedDict`.
    """
    with open("../output_tar/merge.info") as f:
        txt = f.read()

    header = list(map(int, txt.splitlines()[0].split()))
    output = collections.OrderedDict()
    for line in txt.splitlines()[1:]:
        cols = line.split()
        row_num = int(cols[0].split(",")[0])
        vals = [(header[i], int(c.split(",")[1])) for i, c in enumerate(cols)]
        output[row_num] = vals
    return output
