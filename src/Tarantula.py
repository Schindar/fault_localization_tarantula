from __future__ import division
import collections
from DataExtraction import *


def suspiciousness(failed, total_failed, passed, total_passed):
    """
   the suspiciousness of a coverage entity.
   Parameters:
     failed - is the number of failed test cases that executed statement one or more times.
     total_failed - is the total numbers of test cases that fail.
     passed - is the number of passed test cases that executed statement one or more times. 
     total_passed - is the total numbers of test cases that pass.
   The return type is `double`.     
   """
    # there is no safe cases therefore the algorithm does not provide good results => all the positions are buggy
    if total_passed == 0:
        return 1.0
    # there is no failed cases therefore the algorithm does not provide good results => all the positions are safe
    if  total_failed == 0:
        return 0.0

    numerator = failed / total_failed
    denominator = (passed / total_passed) + (failed / total_failed)
    # avoiding divide by zero.
    if denominator == 0:
        return 0.0
    return numerator / denominator


def total_failed(list_of_dic):
    """
    calculate the total failed from list of `OrderedDict`.
    Parameters:
        list_of_dic - is list of `OrderedDict`.
    The return type is `int`.
    """
    one_counter = 0
    for i in range(len(list_of_dic[0][1])):
        if list_of_dic[1][1][i][0] == 1:
            one_counter += 1
    return one_counter


def total_passed(list_of_dic):
    """
    calculate the total passed from list of `OrderedDict`.
    Parameters:
        list_of_dic - is list of `OrderedDict`.
    The return type is `int`.
    """
    zero_counter = 0
    for i in range(len(list_of_dic[0][1])):
        if list_of_dic[1][1][i][0] != 1:
            zero_counter += 1
    return zero_counter


def failed(line_number, list_of_dic):
    """
    calculate failed test cases from list of `OrderedDict`.
    Parameters:
        list_of_dic - is list of `OrderedDict`.
        line_number - number of line for which the failed test will be calculated.
    The return type is `int`.
    """
    failed_counter = 0
    for i in range(len(list_of_dic[find_line_index(line_number, list_of_dic)][1])):
        if (
            list_of_dic[line_number][1][i][0] == 1
            and list_of_dic[line_number][1][i][1] == 1
        ):
            failed_counter += 1
    return failed_counter


def passed(line_number, list_of_dic):
    """
    calculate passed test cases from list of `OrderedDict`.
    Parameters:
        list_of_dic - is list of `OrderedDict`.
        line_number - number of line for which the pass test will be calculated.
    The return type is `int`.
    """
    passed_counter = 0
    for i in range(len(list_of_dic[find_line_index(line_number, list_of_dic)][1])):
        if (
            list_of_dic[line_number][1][i][0] != 1
            and list_of_dic[line_number][1][i][1] == 1
        ):
            passed_counter += 1
    return passed_counter


def find_line_index(line_number, list_of_dic):
    """
    find index of the code-line from list of `OrderedDict`.
    Parameters:
        list_of_dic - is list of `OrderedDict`.
        line_number - number of code-line.
    The return type is `int`.
    """
    for i in range(len(list_of_dic)):
        if line_number == list_of_dic[i][0]:
            return i
    return -1


def find_line_by_index(index, list_of_dic):
    """
   find the code-line by its index from list of `OrderedDict`.
   Parameters:
       list_of_dic - is list of `OrderedDict`.
       index - index of the code-line.
   The return type is `int`.
   """
    return list_of_dic[index][0]


def make_ranking(line_number, list_of_dic):
    """
   make a ranking of probability from list of `OrderedDict`.
   Parameters:
       list_of_dic - is list of `OrderedDict`.
       line_number - number of code-line.
   The return type is `double`.
   """
    
    return suspiciousness(
        failed(line_number, list_of_dic),
        total_failed(list_of_dic),
        passed(line_number, list_of_dic),
        total_passed(list_of_dic),
    )


def main():
    data_extrac()
    resultMap = {}
    list_of_dic = list(make_data_struc().items())

    print("Code-line  --> suspiciousness")
    for i in range(len(list_of_dic)):
        resultMap[find_line_by_index(i, list_of_dic)]=make_ranking(i, list_of_dic)
        #sorting by value
    sortedDic = sorted(resultMap.items(), key=lambda x: x[1], reverse=True)
    file = open(specific + "Tarantula_result.txt","w")
    for item in sortedDic:
        file.write(str(item))
        file.write("\n")
    file.write("total Passed --> "+ str(total_passed(list_of_dic)))
    file.write("\n")
    file.write("total Failed --> " + str(total_failed(list_of_dic)))
    file.close()
    
    for i in sortedDic:
        print(i[0], i[1])


if __name__ == "__main__":
    main()
