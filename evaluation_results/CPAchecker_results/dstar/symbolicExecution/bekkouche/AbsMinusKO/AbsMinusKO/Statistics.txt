
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     6.10 (sum: 372, count: 61, min: 0, max: 8)
Number of global variables per state:              0.00 (sum: 0, count: 61, min: 0, max: 0)
Number of assumptions:                                   46
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                      60
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.010s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.009s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          0.271s
  Time for independent computation:                    0.008s
  Time for SMT check:                                  0.020s
  Time for resolving definites:                        0.037s

Cache lookups:                                          241
Direct cache hits:                                       11
Direct cache lookup time:                              0.001s
Subset cache hits:                                        0
Subset cache lookup time:                              0.010s
Superset cache hits:                                      0
Superset cache lookup time:                            0.012s

Number of removed outdated constraints:                   0 (count: 28, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.026s

Constraints after refinement in state:                  177 (count: 60, min: 0, max: 5, avg: 2.95)
Constraints before refinement in state:                 177 (count: 60, min: 0, max: 5, avg: 2.95)
Time for constraints adjustment:                       0.002s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.012s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 76, count: 76, min: 1, max: 1) [1 x 76]
Number of states with assumption transitions:      0

Fault Localization With DStar statistics
----------------------------------------
DStar total time:                                      1.925s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          12
  Total lines:            13
  Line coverage:          0.923
  Visited conditions:     15
  Total conditions:       16
  Condition coverage:     0.938

CPAchecker general statistics
-----------------------------
Number of program locations:                       43
Number of CFA edges (per node):                          50 (count: 43, min: 0, max: 2, avg: 1.16)
Number of relevant variables:                      8
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             61
  Number of reached locations:   34 (79%)
    Avg states per location:     1
    Max states per location:     4 (at node N1)
  Number of reached functions:   3 (100%)
  Number of partitions:          44
    Avg size of partitions:      1
    Max size of partitions:      4 (with key [N28 (before line 39), Function foo called from node N36, stack depth 2 [7dac3fd8], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       4

Time for analysis setup:          3.603s
  Time for loading CPAs:          1.268s
  Time for loading parser:        0.482s
  Time for CFA construction:      1.654s
    Time for parsing file(s):     0.912s
    Time for AST to CFA:          0.320s
    Time for CFA sanity check:    0.110s
    Time for post-processing:     0.220s
    Time for CFA export:          1.701s
      Time for function pointers resolving:            0.013s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.123s
        Time for collecting variables:                 0.042s
        Time for solving dependencies:                 0.001s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.073s
        Time for exporting data:                       0.007s
Time for Analysis:                1.976s
CPU time for analysis:            1.770s
Time for analyzing result:        0.001s
Total time for CPAchecker:        5.602s
Total CPU time for CPAchecker:    5.040s
Time for statistics:              0.217s

Time for Garbage Collector:       0.186s (in 7 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 32MB (    30 MiB) max;     20MB (    19 MiB) avg;     37MB (    36 MiB) peak
Used non-heap memory:             50MB (    48 MiB) max;     36MB (    34 MiB) avg;     51MB (    49 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        55MB (    53 MiB) max;     40MB (    38 MiB) avg
Total process virtual memory:   3426MB (  3267 MiB) max;   3399MB (  3241 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 21) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
