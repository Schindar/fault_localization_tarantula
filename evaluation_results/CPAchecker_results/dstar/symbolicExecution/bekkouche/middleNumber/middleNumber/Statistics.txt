
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     7.00 (sum: 910, count: 130, min: 0, max: 9)
Number of global variables per state:              0.00 (sum: 0, count: 130, min: 0, max: 0)
Number of assumptions:                                   54
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                     129
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.010s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.002s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          0.497s
  Time for independent computation:                    0.020s
  Time for SMT check:                                  0.058s
  Time for resolving definites:                        0.104s

Cache lookups:                                         1325
Direct cache hits:                                        1
Direct cache lookup time:                              0.001s
Subset cache hits:                                        0
Subset cache lookup time:                              0.011s
Superset cache hits:                                      0
Superset cache lookup time:                            0.022s

Number of removed outdated constraints:                   0 (count: 54, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.065s

Constraints after refinement in state:                  549 (count: 129, min: 0, max: 7, avg: 4.26)
Constraints before refinement in state:                 549 (count: 129, min: 0, max: 7, avg: 4.26)
Time for constraints adjustment:                       0.015s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.023s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 178, count: 178, min: 1, max: 1) [1 x 178]
Number of states with assumption transitions:      0

Fault Localization With DStar statistics
----------------------------------------
DStar total time:                                      2.128s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          36
  Total lines:            36
  Line coverage:          1.000
  Visited conditions:     22
  Total conditions:       22
  Condition coverage:     1.000

CPAchecker general statistics
-----------------------------
Number of program locations:                       70
Number of CFA edges (per node):                          80 (count: 70, min: 0, max: 2, avg: 1.14)
Number of relevant variables:                      16
Number of functions:                               4
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             130
  Number of reached locations:   52 (74%)
    Avg states per location:     2
    Max states per location:     6 (at node N0)
  Number of reached functions:   4 (100%)
  Number of partitions:          100
    Avg size of partitions:      1
    Max size of partitions:      6 (with key [N28 (before line 63), Function worngMid called from node N18, stack depth 2 [443dbe42], stack [main, worngMid]])
  Number of target states:       1
  Size of final wait list:       4

Time for analysis setup:          3.276s
  Time for loading CPAs:          1.201s
  Time for loading parser:        0.516s
  Time for CFA construction:      1.373s
    Time for parsing file(s):     0.684s
    Time for AST to CFA:          0.237s
    Time for CFA sanity check:    0.063s
    Time for post-processing:     0.254s
    Time for CFA export:          1.564s
      Time for function pointers resolving:            0.016s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.140s
        Time for collecting variables:                 0.043s
        Time for solving dependencies:                 0.001s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.073s
        Time for exporting data:                       0.018s
Time for Analysis:                2.174s
CPU time for analysis:            2.050s
Time for analyzing result:        0.006s
Total time for CPAchecker:        5.478s
Total CPU time for CPAchecker:    5.140s
Time for statistics:              0.192s

Time for Garbage Collector:       0.120s (in 7 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 31MB (    29 MiB) max;     20MB (    19 MiB) avg;     38MB (    36 MiB) peak
Used non-heap memory:             51MB (    49 MiB) max;     37MB (    35 MiB) avg;     52MB (    49 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        55MB (    53 MiB) max;     41MB (    39 MiB) avg
Total process virtual memory:   3421MB (  3263 MiB) max;   3399MB (  3241 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 10) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
