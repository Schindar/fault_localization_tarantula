
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     14.62 (sum: 96231, count: 6580, min: 0, max: 16)
Number of global variables per state:              0.00 (sum: 0, count: 6580, min: 0, max: 0)
Number of assumptions:                                 9382
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                    6579
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.148s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.033s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                        450.941s
  Time for independent computation:                    0.635s
  Time for SMT check:                                 84.361s
  Time for resolving definites:                       52.610s

Cache lookups:                                     68953896
Direct cache hits:                                       15
Direct cache lookup time:                              5.655s
Subset cache hits:                                        0
Subset cache lookup time:                            254.768s
Superset cache hits:                                      0
Superset cache lookup time:                          141.697s

Number of removed outdated constraints:                   0 (count: 9358, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  1.039s

Constraints after refinement in state:                99157 (count: 6579, min: 0, max: 23, avg: 15.07)
Constraints before refinement in state:               99157 (count: 6579, min: 0, max: 23, avg: 15.07)
Time for constraints adjustment:                       0.073s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.168s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 10877, count: 10877, min: 1, max: 1) [1 x 10877]
Number of states with assumption transitions:      0

Fault Localization With DStar statistics
----------------------------------------
DStar total time:                                    457.746s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          71
  Total lines:            73
  Line coverage:          0.973
  Visited conditions:     165
  Total conditions:       176
  Condition coverage:     0.938

CPAchecker general statistics
-----------------------------
Number of program locations:                       196
Number of CFA edges (per node):                         283 (count: 196, min: 0, max: 2, avg: 1.44)
Number of relevant variables:                      16
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             6580
  Number of reached locations:   182 (93%)
    Avg states per location:     36
    Max states per location:     377 (at node N13)
  Number of reached functions:   3 (100%)
  Number of partitions:          216
    Avg size of partitions:      30
    Max size of partitions:      377 (with key [N13 (before line 185), Function foo called from node N212, stack depth 2 [3abd581e], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       386

Time for analysis setup:          3.672s
  Time for loading CPAs:          1.072s
  Time for loading parser:        0.458s
  Time for CFA construction:      1.926s
    Time for parsing file(s):     0.729s
    Time for AST to CFA:          0.495s
    Time for CFA sanity check:    0.141s
    Time for post-processing:     0.443s
    Time for CFA export:          1.659s
      Time for function pointers resolving:            0.021s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.273s
        Time for collecting variables:                 0.115s
        Time for solving dependencies:                 0.001s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.131s
        Time for exporting data:                       0.012s
Time for Analysis:              457.820s
CPU time for analysis:          455.050s
Time for analyzing result:        0.019s
Total time for CPAchecker:      461.519s
Total CPU time for CPAchecker:  458.250s
Time for statistics:              0.254s

Time for Garbage Collector:       2.931s (in 688 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                156MB (   149 MiB) max;     85MB (    81 MiB) avg;    158MB (   151 MiB) peak
Used non-heap memory:             55MB (    53 MiB) max;     49MB (    47 MiB) avg;     57MB (    55 MiB) peak
Allocated heap memory:           209MB (   200 MiB) max;    128MB (   122 MiB) avg
Allocated non-heap memory:        60MB (    57 MiB) max;     54MB (    52 MiB) avg
Total process virtual memory:   3495MB (  3333 MiB) max;   3463MB (  3302 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 20) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
