
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     7.81 (sum: 1733, count: 222, min: 0, max: 10)
Number of global variables per state:              0.00 (sum: 0, count: 222, min: 0, max: 0)
Number of assumptions:                                  184
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                     235
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.026s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.000s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          0.288s
  Time for independent computation:                    0.013s
  Time for SMT check:                                  0.023s
  Time for resolving definites:                        0.022s

Cache lookups:                                          101
Direct cache hits:                                       65
Direct cache lookup time:                              0.003s
Subset cache hits:                                        0
Subset cache lookup time:                              0.002s
Superset cache hits:                                      0
Superset cache lookup time:                            0.000s

Number of removed outdated constraints:                  16 (count: 70, min: 0, max: 2, avg: 0.23)
Time for outdated constraint removal:                  0.009s

Constraints after refinement in state:                  349 (count: 235, min: 0, max: 3, avg: 1.49)
Constraints before refinement in state:                 349 (count: 235, min: 0, max: 3, avg: 1.49)
Time for constraints adjustment:                       0.029s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.057s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 261, count: 261, min: 1, max: 1) [1 x 261]
Number of states with assumption transitions:      0

Fault Localization With DStar statistics
----------------------------------------
DStar total time:                                      6.478s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          19
  Total lines:            19
  Line coverage:          1.000
  Visited conditions:     11
  Total conditions:       12
  Condition coverage:     0.917

CPAchecker general statistics
-----------------------------
Number of program locations:                       48
Number of CFA edges (per node):                          53 (count: 48, min: 0, max: 2, avg: 1.10)
Number of relevant variables:                      7
Number of functions:                               3
Number of loops (and loop nodes):                         3 (sum: 25, min: 4, max: 14, avg: 8.33)
Size of reached set:             222
  Number of reached locations:   33 (69%)
    Avg states per location:     6
    Max states per location:     17 (at node N38)
  Number of reached functions:   3 (100%)
  Number of partitions:          103
    Avg size of partitions:      2
    Max size of partitions:      17 (with key [N38 (before line 25), Function main called from node N13, stack depth 1 [70e659aa], stack [main]])
  Number of target states:       1
  Size of final wait list:       4

Time for analysis setup:          3.725s
  Time for loading CPAs:          1.293s
  Time for loading parser:        0.564s
  Time for CFA construction:      1.664s
    Time for parsing file(s):     0.839s
    Time for AST to CFA:          0.204s
    Time for CFA sanity check:    0.081s
    Time for post-processing:     0.237s
    Time for CFA export:          1.625s
      Time for function pointers resolving:            0.006s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.126s
        Time for collecting variables:                 0.038s
        Time for solving dependencies:                 0.013s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.067s
        Time for exporting data:                       0.008s
Time for Analysis:                6.533s
CPU time for analysis:            5.080s
Time for analyzing result:        0.011s
Total time for CPAchecker:       10.277s
Total CPU time for CPAchecker:    8.220s
Time for statistics:              0.185s

Time for Garbage Collector:       0.195s (in 11 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 45MB (    43 MiB) max;     25MB (    24 MiB) avg;     51MB (    48 MiB) peak
Used non-heap memory:             60MB (    57 MiB) max;     45MB (    42 MiB) avg;     61MB (    58 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        64MB (    61 MiB) max;     49MB (    47 MiB) avg
Total process virtual memory:   3428MB (  3269 MiB) max;   3411MB (  3253 MiB) avg

Verification result: FALSE. Property violation (unreach-call:  called in line 6) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
