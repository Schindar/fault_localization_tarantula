2020-06-10 22:27:41:823	INFO	CPAMain.detectFrontendLanguageIfNecessary	Language C detected and set for analysis

2020-06-10 22:27:41:922	INFO	ResourceLimitChecker.fromConfiguration	Using the following resource limits: CPU-time limit of 900s

2020-06-10 22:27:42:127	INFO	CPAchecker.run	CPAchecker 1.9.2-svn / symbolicExecution (OpenJDK 64-Bit Server VM 11.0.7) started

2020-06-10 22:27:42:197	INFO	CPAchecker.parse	Parsing CFA from file(s) "test/programs/tarantula/benchmarks/bekkouche_benchmark/tritype/TritypeKO2V2.c"

2020-06-10 22:27:43:842	WARNING	CheckBindingVisitor.visit	Undefined function __VERIFIER_nondet_int found, first called in line 97

2020-06-10 22:27:45:415	INFO	CPAchecker.runAlgorithm	Starting analysis ...

2020-06-10 22:27:47:558	INFO	CounterexampleCheckAlgorithm.checkCounterexample	Error path found, starting counterexample check with CPACHECKER.

2020-06-10 22:27:48:088	INFO	CounterexampleCheck:ResourceLimitChecker.fromConfiguration	Using the following resource limits: CPU-time limit of 900s

2020-06-10 22:27:48:419	INFO	CounterexampleCheck:PredicateCPA:PredicateCPA.<init>	Using predicate analysis with MathSAT5 version 5.5.4 (bd863fede57e) (Feb 21 2019 15:05:40, gmp 6.1.0, gcc 4.8.5, 64-bit, reentrant).

2020-06-10 22:27:48:542	WARNING	CounterexampleCheck:PredicateCPA:CtoFormulaConverter.getReturnType	line 97: Return type of function __VERIFIER_nondet_int is void, but result is used as type int: __CPAchecker_TMP_0 = __VERIFIER_nondet_int();

2020-06-10 22:27:48:548	WARNING	CounterexampleCheck:PredicateCPA:CtoFormulaConverter.getReturnType	line 97: Return type of function __VERIFIER_nondet_int is void, but result is used as type int: __CPAchecker_TMP_1 = __VERIFIER_nondet_int();

2020-06-10 22:27:48:566	WARNING	CounterexampleCheck:PredicateCPA:CtoFormulaConverter.getReturnType	line 97: Return type of function __VERIFIER_nondet_int is void, but result is used as type int: __CPAchecker_TMP_2 = __VERIFIER_nondet_int();

2020-06-10 22:27:48:771	INFO	CounterexampleCheckAlgorithm.checkCounterexample	Error path found and confirmed by counterexample check with CPACHECKER.

2020-06-10 22:27:48:782	INFO	TarantulaAlgorithm.run	Start tarantula algorithm ... 

2020-06-10 22:27:48:923	INFO	TarantulaAlgorithm.getFaultLocations	[Error suspected on line(s): 24 (Score: 100). (Score: 100)
       HINT: Unknown potential fault at Edge ( [cond == 0] ) with suspicious ( 1.0 )  (0.0%)
, Error suspected on line(s): 25 (Score: 100). (Score: 100)
       HINT: Unknown potential fault at Edge ( Label: ERROR ) with suspicious ( 1.0 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __VERIFIER_error(); ) with suspicious ( 1.0 )  (0.0%)
, Error suspected on line(s): 90 (Score: 93). (Score: 93)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_1; ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(i == 0)] ) with suspicious ( 0.5172413793103449 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(j == 0)] ) with suspicious ( 0.5357142857142857 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(k == 0)] ) with suspicious ( 0.5555555555555556 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_3; ) with suspicious ( 0.5555555555555556 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_9; ) with suspicious ( 0.6521739130434783 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(j == k)] ) with suspicious ( 0.75 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_11; ) with suspicious ( 0.6818181818181818 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_13; ) with suspicious ( 0.7142857142857143 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_11 = __CPAchecker_TMP_13; ) with suspicious ( 0.7142857142857143 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_9 = __CPAchecker_TMP_11; ) with suspicious ( 0.6818181818181818 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_3 = __CPAchecker_TMP_9; ) with suspicious ( 0.7142857142857143 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [i != j] ) with suspicious ( 0.625 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_1 = __CPAchecker_TMP_3; ) with suspicious ( 0.6521739130434783 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __VERIFIER_assert(trityp == __CPAchecker_TMP_1) ) with suspicious ( 0.5769230769230769 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(i == j)] ) with suspicious ( 0.75 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(i == j)] ) with suspicious ( 0.75 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [i != j] ) with suspicious ( 0.75 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(i != k)] ) with suspicious ( 0.8333333333333334 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [j != k] ) with suspicious ( 0.8333333333333334 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [i == k] ) with suspicious ( 0.8333333333333334 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [(i + k) > j] ) with suspicious ( 0.9375 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_13 = 2; ) with suspicious ( 0.9375 )  (0.0%)
, Error suspected on line(s): 78 (Score: 88). (Score: 88)
       HINT: Unknown potential fault at Edge ( [!(trityp == 3)] ) with suspicious ( 0.8823529411764706 )  (0.0%)
, Error suspected on line(s): 70 (Score: 88). (Score: 88)
       HINT: Unknown potential fault at Edge ( [trityp == 1] ) with suspicious ( 0.75 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!((i + j) > k)] ) with suspicious ( 0.8823529411764706 )  (0.0%)
, Error suspected on line(s): 82 (Score: 83). (Score: 83)
       HINT: Unknown potential fault at Edge ( trityp = 4; ) with suspicious ( 0.8333333333333334 )  (0.0%)
, Error suspected on line(s): 50 (Score: 78). (Score: 78)
       HINT: Unknown potential fault at Edge ( [i == k] ) with suspicious ( 0.7894736842105263 )  (0.0%)
, Error suspected on line(s): 52 (Score: 78). (Score: 78)
       HINT: Unknown potential fault at Edge ( trityp = trityp + 1; ) with suspicious ( 0.7894736842105263 )  (0.0%)
, Error suspected on line(s): 74 (Score: 78). (Score: 78)
       HINT: Unknown potential fault at Edge ( [!(trityp == 2)] ) with suspicious ( 0.7894736842105263 )  (0.0%)
, Error suspected on line(s): 66 (Score: 68). (Score: 68)
       HINT: Unknown potential fault at Edge ( [!(trityp > 3)] ) with suspicious ( 0.6818181818181818 )  (0.0%)
, Error suspected on line(s): 57 (Score: 65). (Score: 65)
       HINT: Unknown potential fault at Edge ( [!(trityp == 0)] ) with suspicious ( 0.6521739130434783 )  (0.0%)
, Error suspected on line(s): 54 (Score: 62). (Score: 62)
       HINT: Unknown potential fault at Edge ( [!(j == k)] ) with suspicious ( 0.625 )  (0.0%)
, Error suspected on line(s): 47 (Score: 62). (Score: 62)
       HINT: Unknown potential fault at Edge ( [!(i == j)] ) with suspicious ( 0.625 )  (0.0%)
, Error suspected on line(s): 46 (Score: 55). (Score: 55)
       HINT: Unknown potential fault at Edge ( trityp = 0; ) with suspicious ( 0.5555555555555556 )  (0.0%)
, Error suspected on line(s): 42 (Score: 55). (Score: 55)
       HINT: Unknown potential fault at Edge ( [!(i == 0)] ) with suspicious ( 0.5172413793103449 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(j == 0)] ) with suspicious ( 0.5357142857142857 )  (0.0%)
       HINT: Unknown potential fault at Edge ( [!(k == 0)] ) with suspicious ( 0.5555555555555556 )  (0.0%)
, Error suspected on line(s): 97 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_0; ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_0 = __VERIFIER_nondet_int(); ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_1; ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_1 = __VERIFIER_nondet_int(); ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( int __CPAchecker_TMP_2; ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( __CPAchecker_TMP_2 = __VERIFIER_nondet_int(); ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge ( foo(__CPAchecker_TMP_0, __CPAchecker_TMP_1, __CPAchecker_TMP_2) ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 23 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void __VERIFIER_assert(int cond); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 19 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int __VERIFIER_nondet_uint(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 20 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void __VERIFIER_error(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 41 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int trityp; ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 94 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int main(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 40 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void foo(int i, int j, int k); ) with suspicious ( 0.5 )  (0.0%)
]

2020-06-10 22:27:48:958	INFO	CPAchecker.runAlgorithm	Stopping analysis ...

