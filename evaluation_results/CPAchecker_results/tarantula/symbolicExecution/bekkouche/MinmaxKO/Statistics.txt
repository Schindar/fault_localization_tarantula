
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     7.66 (sum: 651, count: 85, min: 0, max: 9)
Number of global variables per state:              0.00 (sum: 0, count: 85, min: 0, max: 0)
Number of assumptions:                                   44
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                      84
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.029s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.011s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          0.472s
  Time for independent computation:                    0.018s
  Time for model re-use attempts:                      0.082s
  Time for SMT check:                                  0.017s
  Time for resolving definites:                        0.094s
Successful model re-uses:                                16

Cache lookups:                                           44
Direct cache hits:                                        0
Direct cache lookup time:                              0.001s
Subset cache hits:                                        0
Subset cache lookup time:                              0.009s

Number of removed outdated constraints:                   0 (count: 44, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.023s

Constraints after refinement in state:                  282 (count: 84, min: 0, max: 5, avg: 3.36)
Constraints before refinement in state:                 282 (count: 84, min: 0, max: 5, avg: 3.36)
Time for constraints adjustment:                       0.018s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.022s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 107, count: 107, min: 1, max: 1) [1 x 107]
Number of states with assumption transitions:      0

Fault Localization With Tarantula statistics
--------------------------------------------
Tarantula total time:                                  2.016s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          17
  Total lines:            18
  Line coverage:          0.944
  Visited conditions:     10
  Total conditions:       10
  Condition coverage:     1.000

CPAchecker general statistics
-----------------------------
Number of program locations:                       43
Number of CFA edges (per node):                          47 (count: 43, min: 0, max: 2, avg: 1.09)
Number of relevant variables:                      9
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             85
  Number of reached locations:   31 (72%)
    Avg states per location:     2
    Max states per location:     9 (at node N1)
  Number of reached functions:   3 (100%)
  Number of partitions:          58
    Avg size of partitions:      1
    Max size of partitions:      9 (with key [N24 (before line 43), Function foo called from node N36, stack depth 2 [48e64352], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       10

Time for analysis setup:          2.985s
  Time for loading CPAs:          1.098s
  Time for loading parser:        0.441s
  Time for CFA construction:      1.278s
    Time for parsing file(s):     0.645s
    Time for AST to CFA:          0.281s
    Time for CFA sanity check:    0.083s
    Time for post-processing:     0.188s
    Time for CFA export:          1.512s
      Time for function pointers resolving:            0.006s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.097s
        Time for collecting variables:                 0.034s
        Time for solving dependencies:                 0.003s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.055s
        Time for exporting data:                       0.005s
Time for Analysis:                2.047s
CPU time for analysis:            1.840s
Time for analyzing result:        0.014s
Total time for CPAchecker:        5.049s
Total CPU time for CPAchecker:    4.770s
Time for statistics:              0.212s

Time for Garbage Collector:       0.109s (in 7 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 31MB (    29 MiB) max;     20MB (    19 MiB) avg;     37MB (    36 MiB) peak
Used non-heap memory:             51MB (    48 MiB) max;     37MB (    35 MiB) avg;     52MB (    49 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        56MB (    53 MiB) max;     41MB (    39 MiB) avg
Total process virtual memory:   3432MB (  3273 MiB) max;   3408MB (  3250 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 22) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
