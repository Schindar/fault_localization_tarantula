
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     9.62 (sum: 3848, count: 400, min: 0, max: 14)
Number of global variables per state:              0.00 (sum: 0, count: 400, min: 0, max: 0)
Number of assumptions:                                  398
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                     399
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.049s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.016s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          1.207s
  Time for independent computation:                    0.063s
  Time for model re-use attempts:                      0.236s
  Time for SMT check:                                  0.025s
  Time for resolving definites:                        0.266s
Successful model re-uses:                                12

Cache lookups:                                          583
Direct cache hits:                                      181
Direct cache lookup time:                              0.004s
Subset cache hits:                                        0
Subset cache lookup time:                              0.050s

Number of removed outdated constraints:                   0 (count: 326, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.121s

Constraints after refinement in state:                 2570 (count: 399, min: 0, max: 10, avg: 6.44)
Constraints before refinement in state:                2570 (count: 399, min: 0, max: 10, avg: 6.44)
Time for constraints adjustment:                       0.020s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.052s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 552, count: 552, min: 1, max: 1) [1 x 552]
Number of states with assumption transitions:      0

Fault Localization With Tarantula statistics
--------------------------------------------
Tarantula total time:                                  3.709s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          33
  Total lines:            33
  Line coverage:          1.000
  Visited conditions:     85
  Total conditions:       92
  Condition coverage:     0.924

CPAchecker general statistics
-----------------------------
Number of program locations:                       122
Number of CFA edges (per node):                         167 (count: 122, min: 0, max: 2, avg: 1.37)
Number of relevant variables:                      16
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             400
  Number of reached locations:   111 (91%)
    Avg states per location:     3
    Max states per location:     14 (at node N13)
  Number of reached functions:   3 (100%)
  Number of partitions:          145
    Avg size of partitions:      2
    Max size of partitions:      14 (with key [N13 (before line 94), Function foo called from node N120, stack depth 2 [12f3afb5], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       5

Time for analysis setup:          3.621s
  Time for loading CPAs:          1.199s
  Time for loading parser:        0.455s
  Time for CFA construction:      1.768s
    Time for parsing file(s):     0.736s
    Time for AST to CFA:          0.527s
    Time for CFA sanity check:    0.084s
    Time for post-processing:     0.289s
    Time for CFA export:          1.722s
      Time for function pointers resolving:            0.005s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.168s
        Time for collecting variables:                 0.057s
        Time for solving dependencies:                 0.000s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.092s
        Time for exporting data:                       0.019s
Time for Analysis:                3.764s
CPU time for analysis:            3.490s
Time for analyzing result:        0.001s
Total time for CPAchecker:        7.434s
Total CPU time for CPAchecker:    6.810s
Time for statistics:              0.186s

Time for Garbage Collector:       0.142s (in 9 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 34MB (    33 MiB) max;     22MB (    21 MiB) avg;     38MB (    36 MiB) peak
Used non-heap memory:             55MB (    52 MiB) max;     40MB (    38 MiB) avg;     56MB (    53 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        59MB (    56 MiB) max;     44MB (    42 MiB) avg
Total process virtual memory:   3435MB (  3276 MiB) max;   3412MB (  3254 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 25) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
