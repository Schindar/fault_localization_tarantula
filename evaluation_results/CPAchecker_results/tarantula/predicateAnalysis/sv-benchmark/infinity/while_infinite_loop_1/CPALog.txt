2020-06-10 23:02:18:071	INFO	CPAMain.detectFrontendLanguageIfNecessary	Language C detected and set for analysis

2020-06-10 23:02:18:088	INFO	ResourceLimitChecker.fromConfiguration	Using the following resource limits: CPU-time limit of 900s

2020-06-10 23:02:18:184	INFO	CPAchecker.run	CPAchecker 1.9.2-svn / predicateAnalysis (OpenJDK 64-Bit Server VM 11.0.2) started

2020-06-10 23:02:18:216	INFO	CPAchecker.parse	Parsing CFA from file(s) "test/programs/tarantula/benchmarks/sv-benchmark/infinity/while_infinite_loop_1.c"

2020-06-10 23:02:18:831	INFO	CFACreationUtils.addEdgeToCFA	line 19: Dead code detected: __VERIFIER_assert(x!=0);

2020-06-10 23:02:19:244	WARNING	PredicateCPA:FormulaManagerView.<init>	Using unsound approximation of ints with unbounded integers and floats with rationals for encoding program semantics.

2020-06-10 23:02:19:310	INFO	PredicateCPA:PredicateCPA.<init>	Using predicate analysis with SMTInterpol 2.5-604-g71e72f93 and JFactory 1.21.

2020-06-10 23:02:19:363	WARNING	ARGCPA:FormulaManagerView.<init>	Using unsound approximation of ints with unbounded integers and floats with rationals for encoding program semantics.

2020-06-10 23:02:19:371	WARNING	ARGCPA:FormulaManagerView.<init>	Using unsound approximation of ints with unbounded integers and floats with rationals for encoding program semantics.

2020-06-10 23:02:19:461	INFO	PredicateCPA:PredicateCPARefiner.<init>	Using refinement for predicate analysis with PredicateAbstractionRefinementStrategy strategy.

2020-06-10 23:02:19:485	INFO	CPAchecker.runAlgorithm	Starting analysis ...

2020-06-10 23:02:19:605	INFO	TarantulaAlgorithm.run	Start tarantula algorithm ... 

2020-06-10 23:02:19:624	INFO	TarantulaAlgorithm.getFaultLocations	[Error suspected on line(s): 5 (Score: 100). (Score: 100)
       HINT: Unknown potential fault at Edge ( [cond == 0] ) with suspicious ( 1.0 )  (0.0%)
, Error suspected on line(s): 6 (Score: 100). (Score: 100)
       HINT: Unknown potential fault at Edge ( Label: ERROR ) with suspicious ( 1.0 )  (0.0%)
, Error suspected on line(s): 1 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void abort(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 16 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( __VERIFIER_assert(x == 8) ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 2 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void reach_error(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 11 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int main(); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 14 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( while ) with suspicious ( 0.5 )  (0.0%)
       HINT: Unknown potential fault at Edge (  ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 4 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( void __VERIFIER_assert(int cond); ) with suspicious ( 0.5 )  (0.0%)
, Error suspected on line(s): 12 (Score: 50). (Score: 50)
       HINT: Unknown potential fault at Edge ( int x = 0; ) with suspicious ( 0.5 )  (0.0%)
]

2020-06-10 23:02:19:627	INFO	CPAchecker.runAlgorithm	Stopping analysis ...

