
PredicateCPA statistics
-----------------------
Number of abstractions:            30 (4% of all post computations)
  Times abstraction was reused:    0
  Because of function entry/exit:  0 (0%)
  Because of loop head:            0 (0%)
  Because of join nodes:           0 (0%)
  Because of threshold:            0 (0%)
  Because of target state:         30 (100%)
  Times precision was empty:       1 (3%)
  Times precision was {false}:     0 (0%)
  Times result was cached:         0 (0%)
  Times cartesian abs was used:    0 (0%)
  Times boolean abs was used:      29 (97%)
  Times result was 'false':        28 (93%)
Number of strengthen sat checks:   0
Number of coverage checks:         2130
  BDD entailment checks:           0
Number of SMT sat checks:          0
  trivial:                         0
  cached:                          0

Max ABE block size:                       28
Avg ABE block size:                                26.70 (sum: 801, count: 30, min: 26, max: 28)
Number of predicates discovered:          1
Number of abstraction locations:          0
Max number of predicates per location:    0
Avg number of predicates per location:    0
Total predicates per abstraction:         29
Max number of predicates per abstraction: 1
Avg number of predicates per abstraction: 1.00
Number of irrelevant predicates:          0 (0%)
Number of preds handled by boolean abs:   29 (100%)
  Total number of models for allsat:      1
  Max number of models for allsat:        1
  Avg number of models for allsat:        0.03

Time for post operator:                                0.404s
  Time for path formula creation:                      0.398s
Time for strengthen operator:                          0.061s
Time for prec operator:                                0.188s
  Time for abstraction:                  0.165s (Max:     0.022s, Count: 30)
    Boolean abstraction:                 0.053s
    Solving time:                        0.032s (Max:     0.020s)
    Model enumeration time:              0.000s
    Time for BDD construction:           0.001s (Max:     0.001s)
Time for coverage checks:                              0.002s
Total time for SMT solver (w/o itp):     0.032s

Number of path formula cache hits:   321 (39%)

Inside post operator:                  
  Inside path formula creation:        
    Time for path formula computation:     0.398s

Total number of created targets for pointer analysis: 0


Number of BDD nodes:                               202
Size of BDD node table:                            60821
Size of BDD cache:                                 6089
Size of BDD node cleanup queue:                    0.07 (sum: 13, count: 178, min: 0, max: 13)
Time for BDD node cleanup:                             0.000s
Time for BDD garbage collection:                       0.000s (in 0 runs)

KeyValue statistics
-------------------
Init. function predicates:                         0
Init. global predicates:                           0
Init. location predicates:                         0

Invariant Generation statistics
-------------------------------

AutomatonAnalysis (AssertionAutomaton) statistics
-------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.025s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 784, count: 784, min: 1, max: 1) [1 x 784]
Number of states with assumption transitions:      0

AutomatonAnalysis (ErrorLabelAutomaton) statistics
--------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.028s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 784, count: 784, min: 1, max: 1) [1 x 784]
Number of states with assumption transitions:      0

AutomatonAnalysis (TerminatingFunctions) statistics
---------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.010s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 784, count: 784, min: 1, max: 1) [1 x 784]
Number of states with assumption transitions:      0

Fault Localization With Tarantula statistics
--------------------------------------------
Tarantula total time:                                  1.710s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          35
  Total lines:            36
  Line coverage:          0.972
  Visited conditions:     22
  Total conditions:       22
  Condition coverage:     1.000

CPAchecker general statistics
-----------------------------
Number of program locations:                       70
Number of CFA edges (per node):                          80 (count: 70, min: 0, max: 2, avg: 1.14)
Number of relevant variables:                      16
Number of functions:                               4
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             435
  Number of reached locations:   50 (71%)
    Avg states per location:     8
    Max states per location:     36 (at node N20)
  Number of reached functions:   4 (100%)
  Number of partitions:          284
    Avg size of partitions:      1
    Max size of partitions:      36 (with key [N20 (before line 28), Function main called from node N9, stack depth 1 [40e4ea87], stack [main], ABS0: true])
  Number of target states:       1
  Size of final wait list:       37

Time for analysis setup:          3.961s
  Time for loading CPAs:          1.558s
  Time for loading parser:        0.490s
  Time for CFA construction:      1.510s
    Time for parsing file(s):     0.923s
    Time for AST to CFA:          0.206s
    Time for CFA sanity check:    0.065s
    Time for post-processing:     0.208s
    Time for CFA export:          1.767s
      Time for function pointers resolving:            0.003s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.117s
        Time for collecting variables:                 0.044s
        Time for solving dependencies:                 0.000s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.057s
        Time for exporting data:                       0.016s
Time for Analysis:                1.724s
CPU time for analysis:            1.490s
Time for analyzing result:        0.003s
Total time for CPAchecker:        5.693s
Total CPU time for CPAchecker:    4.940s
Time for statistics:              0.382s

Time for Garbage Collector:       0.141s (in 8 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 34MB (    33 MiB) max;     21MB (    20 MiB) avg;     39MB (    38 MiB) peak
Used non-heap memory:             49MB (    47 MiB) max;     36MB (    34 MiB) avg;     51MB (    48 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        54MB (    51 MiB) max;     40MB (    38 MiB) avg
Total process virtual memory:   3429MB (  3270 MiB) max;   3406MB (  3248 MiB) avg

Verification result: FALSE. Property violation (error label in line 8) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
