
PredicateCPA statistics
-----------------------
Number of abstractions:            6 (3% of all post computations)
  Times abstraction was reused:    0
  Because of function entry/exit:  0 (0%)
  Because of loop head:            0 (0%)
  Because of join nodes:           0 (0%)
  Because of threshold:            0 (0%)
  Because of target state:         6 (100%)
  Times precision was empty:       1 (17%)
  Times precision was {false}:     0 (0%)
  Times result was cached:         0 (0%)
  Times cartesian abs was used:    0 (0%)
  Times boolean abs was used:      5 (83%)
  Times result was 'false':        4 (67%)
Number of strengthen sat checks:   0
Number of coverage checks:         456
  BDD entailment checks:           0
Number of SMT sat checks:          0
  trivial:                         0
  cached:                          0

Max ABE block size:                       19
Avg ABE block size:                                17.83 (sum: 107, count: 6, min: 17, max: 19)
Number of predicates discovered:          1
Number of abstraction locations:          0
Max number of predicates per location:    0
Avg number of predicates per location:    0
Total predicates per abstraction:         5
Max number of predicates per abstraction: 1
Avg number of predicates per abstraction: 1.00
Number of irrelevant predicates:          0 (0%)
Number of preds handled by boolean abs:   5 (100%)
  Total number of models for allsat:      1
  Max number of models for allsat:        1
  Avg number of models for allsat:        0.20

Time for post operator:                                0.209s
  Time for path formula creation:                      0.206s
Time for strengthen operator:                          0.033s
Time for prec operator:                                0.072s
  Time for abstraction:                  0.067s (Max:     0.022s, Count: 6)
    Boolean abstraction:                 0.023s
    Solving time:                        0.012s (Max:     0.006s)
    Model enumeration time:              0.000s
    Time for BDD construction:           0.001s (Max:     0.001s)
Time for coverage checks:                              0.001s
Total time for SMT solver (w/o itp):     0.012s

Number of path formula cache hits:   113 (50%)

Inside post operator:                  
  Inside path formula creation:        
    Time for path formula computation:     0.185s

Total number of created targets for pointer analysis: 0


Number of BDD nodes:                               202
Size of BDD node table:                            60821
Size of BDD cache:                                 6089
Size of BDD node cleanup queue:                    0.00 (sum: 0, count: 34, min: 0, max: 0)
Time for BDD node cleanup:                             0.001s
Time for BDD garbage collection:                       0.000s (in 0 runs)

KeyValue statistics
-------------------
Init. function predicates:                         0
Init. global predicates:                           0
Init. location predicates:                         0

Invariant Generation statistics
-------------------------------

AutomatonAnalysis (AssertionAutomaton) statistics
-------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.042s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 196, count: 196, min: 1, max: 1) [1 x 196]
Number of states with assumption transitions:      0

AutomatonAnalysis (ErrorLabelAutomaton) statistics
--------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.017s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 196, count: 196, min: 1, max: 1) [1 x 196]
Number of states with assumption transitions:      0

AutomatonAnalysis (TerminatingFunctions) statistics
---------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.024s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 196, count: 196, min: 1, max: 1) [1 x 196]
Number of states with assumption transitions:      0

Fault Localization With Tarantula statistics
--------------------------------------------
Tarantula total time:                                  1.317s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          17
  Total lines:            18
  Line coverage:          0.944
  Visited conditions:     10
  Total conditions:       10
  Condition coverage:     1.000

CPAchecker general statistics
-----------------------------
Number of program locations:                       43
Number of CFA edges (per node):                          47 (count: 43, min: 0, max: 2, avg: 1.09)
Number of relevant variables:                      9
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             99
  Number of reached locations:   29 (67%)
    Avg states per location:     3
    Max states per location:     16 (at node N24)
  Number of reached functions:   3 (100%)
  Number of partitions:          48
    Avg size of partitions:      2
    Max size of partitions:      16 (with key [N24 (before line 43), Function foo called from node N36, stack depth 2 [2b50150], stack [main, foo], ABS0: true])
  Number of target states:       1
  Size of final wait list:       17

Time for analysis setup:          4.373s
  Time for loading CPAs:          1.883s
  Time for loading parser:        0.476s
  Time for CFA construction:      1.677s
    Time for parsing file(s):     0.747s
    Time for AST to CFA:          0.351s
    Time for CFA sanity check:    0.140s
    Time for post-processing:     0.331s
    Time for CFA export:          3.936s
      Time for function pointers resolving:            0.015s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.147s
        Time for collecting variables:                 0.029s
        Time for solving dependencies:                 0.001s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.107s
        Time for exporting data:                       0.010s
Time for Analysis:                1.342s
CPU time for analysis:            1.080s
Time for analyzing result:        0.001s
Total time for CPAchecker:        5.719s
Total CPU time for CPAchecker:    4.470s
Time for statistics:              0.420s

Time for Garbage Collector:       0.156s (in 7 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 32MB (    31 MiB) max;     20MB (    19 MiB) avg;     39MB (    37 MiB) peak
Used non-heap memory:             47MB (    45 MiB) max;     34MB (    33 MiB) avg;     49MB (    46 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        52MB (    49 MiB) max;     39MB (    37 MiB) avg
Total process virtual memory:   3433MB (  3274 MiB) max;   3402MB (  3244 MiB) avg

Verification result: FALSE. Property violation (error label in line 22) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
