
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     14.63 (sum: 96171, count: 6573, min: 0, max: 16)
Number of global variables per state:              0.00 (sum: 0, count: 6573, min: 0, max: 0)
Number of assumptions:                                 9294
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                    6572
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.099s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.039s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                        429.680s
  Time for independent computation:                    0.705s
  Time for SMT check:                                 85.177s
  Time for resolving definites:                       52.424s

Cache lookups:                                     67525600
Direct cache hits:                                       21
Direct cache lookup time:                              5.440s
Subset cache hits:                                        0
Subset cache lookup time:                            233.844s
Superset cache hits:                                      0
Superset cache lookup time:                          134.618s

Number of removed outdated constraints:                   0 (count: 9261, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.934s

Constraints after refinement in state:                99297 (count: 6572, min: 0, max: 23, avg: 15.11)
Constraints before refinement in state:               99297 (count: 6572, min: 0, max: 23, avg: 15.11)
Time for constraints adjustment:                       0.106s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.169s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 10813, count: 10813, min: 1, max: 1) [1 x 10813]
Number of states with assumption transitions:      0

Fault Localization With Ochiai statistics
-----------------------------------------
Ochiai total time:                                   436.401s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          71
  Total lines:            73
  Line coverage:          0.973
  Visited conditions:     168
  Total conditions:       176
  Condition coverage:     0.955

CPAchecker general statistics
-----------------------------
Number of program locations:                       196
Number of CFA edges (per node):                         283 (count: 196, min: 0, max: 2, avg: 1.44)
Number of relevant variables:                      16
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             6573
  Number of reached locations:   182 (93%)
    Avg states per location:     36
    Max states per location:     374 (at node N13)
  Number of reached functions:   3 (100%)
  Number of partitions:          246
    Avg size of partitions:      26
    Max size of partitions:      374 (with key [N13 (before line 186), Function foo called from node N212, stack depth 2 [1dfd5f51], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       393

Time for analysis setup:          3.582s
  Time for loading CPAs:          1.387s
  Time for loading parser:        0.398s
  Time for CFA construction:      1.597s
    Time for parsing file(s):     0.662s
    Time for AST to CFA:          0.294s
    Time for CFA sanity check:    0.149s
    Time for post-processing:     0.385s
    Time for CFA export:          1.841s
      Time for function pointers resolving:            0.016s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.240s
        Time for collecting variables:                 0.097s
        Time for solving dependencies:                 0.001s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.127s
        Time for exporting data:                       0.015s
Time for Analysis:              436.439s
CPU time for analysis:          431.960s
Time for analyzing result:        0.015s
Total time for CPAchecker:      440.040s
Total CPU time for CPAchecker:  435.190s
Time for statistics:              0.269s

Time for Garbage Collector:       2.888s (in 680 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                156MB (   149 MiB) max;     84MB (    80 MiB) avg;    158MB (   151 MiB) peak
Used non-heap memory:             55MB (    52 MiB) max;     49MB (    47 MiB) avg;     57MB (    54 MiB) peak
Allocated heap memory:           210MB (   200 MiB) max;    126MB (   120 MiB) avg
Allocated non-heap memory:        59MB (    57 MiB) max;     54MB (    52 MiB) avg
Total process virtual memory:   3488MB (  3327 MiB) max;   3461MB (  3300 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 21) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
