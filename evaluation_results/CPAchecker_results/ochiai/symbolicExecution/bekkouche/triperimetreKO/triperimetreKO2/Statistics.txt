
ValueAnalysisCPA statistics
---------------------------
Number of variables per state:                     8.79 (sum: 3602, count: 410, min: 0, max: 13)
Number of global variables per state:              0.00 (sum: 0, count: 410, min: 0, max: 0)
Number of assumptions:                                  414
Number of deterministic assumptions:                      0
Level of Determinism:                              0%

ValueAnalysisPrecisionAdjustment statistics
-------------------------------------------
Number of abstraction computations:                     409
Total time for liveness abstraction:                   0.000s
Total time for abstraction computation:                0.033s
Total time for path thresholds:                        0.000s

ConstraintsStrengthenOperator statistics
----------------------------------------
Total time for strengthening by ConstraintsCPA:     0.007s
Replaced symbolic expressions: 0

ConstraintsCPA statistics
-------------------------

Time for solving constraints:                          1.028s
  Time for independent computation:                    0.055s
  Time for SMT check:                                  0.124s
  Time for resolving definites:                        0.209s

Cache lookups:                                        17427
Direct cache hits:                                      214
Direct cache lookup time:                              0.022s
Subset cache hits:                                        0
Subset cache lookup time:                              0.136s
Superset cache hits:                                      0
Superset cache lookup time:                            0.089s

Number of removed outdated constraints:                   0 (count: 330, min: 0, max: 0, avg: 0.00)
Time for outdated constraint removal:                  0.108s

Constraints after refinement in state:                 2658 (count: 409, min: 0, max: 9, avg: 6.50)
Constraints before refinement in state:                2658 (count: 409, min: 0, max: 9, avg: 6.50)
Time for constraints adjustment:                       0.017s

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.053s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 553, count: 553, min: 1, max: 1) [1 x 553]
Number of states with assumption transitions:      0

Fault Localization With Ochiai statistics
-----------------------------------------
Ochiai total time:                                     3.126s

Code Coverage
-----------------------------
  Function coverage:      1.000
  Visited lines:          28
  Total lines:            28
  Line coverage:          1.000
  Visited conditions:     85
  Total conditions:       92
  Condition coverage:     0.924

CPAchecker general statistics
-----------------------------
Number of program locations:                       118
Number of CFA edges (per node):                         163 (count: 118, min: 0, max: 2, avg: 1.38)
Number of relevant variables:                      15
Number of functions:                               3
Number of loops (and loop nodes):                         0 (sum: 0, min: 0, max: 0, avg: 0.00)
Size of reached set:             410
  Number of reached locations:   111 (94%)
    Avg states per location:     3
    Max states per location:     14 (at node N12)
  Number of reached functions:   3 (100%)
  Number of partitions:          145
    Avg size of partitions:      2
    Max size of partitions:      14 (with key [N12 (before line 93), Function foo called from node N116, stack depth 2 [751e664e], stack [main, foo]])
  Number of target states:       1
  Size of final wait list:       6

Time for analysis setup:          3.082s
  Time for loading CPAs:          1.132s
  Time for loading parser:        0.428s
  Time for CFA construction:      1.347s
    Time for parsing file(s):     0.609s
    Time for AST to CFA:          0.305s
    Time for CFA sanity check:    0.124s
    Time for post-processing:     0.237s
    Time for CFA export:          1.499s
      Time for function pointers resolving:            0.008s
        Function calls via function pointers:             0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer calls:              0 (count: 1, min: 0, max: 0, avg: 0.00)
        Function calls with function pointer arguments:        0 (count: 1, min: 0, max: 0, avg: 0.00)
        Instrumented function pointer arguments:          0 (count: 1, min: 0, max: 0, avg: 0.00)
      Time for classifying variables:                  0.151s
        Time for collecting variables:                 0.073s
        Time for solving dependencies:                 0.004s
        Time for building hierarchy:                   0.000s
        Time for building classification:              0.065s
        Time for exporting data:                       0.008s
Time for Analysis:                3.173s
CPU time for analysis:            3.070s
Time for analyzing result:        0.006s
Total time for CPAchecker:        6.298s
Total CPU time for CPAchecker:    6.140s
Time for statistics:              0.189s

Time for Garbage Collector:       0.127s (in 9 runs)
Garbage Collector(s) used:    Copy, MarkSweepCompact
Used heap memory:                 34MB (    32 MiB) max;     22MB (    21 MiB) avg;     38MB (    36 MiB) peak
Used non-heap memory:             55MB (    52 MiB) max;     40MB (    38 MiB) avg;     56MB (    53 MiB) peak
Allocated heap memory:            75MB (    71 MiB) max;     75MB (    71 MiB) avg
Allocated non-heap memory:        59MB (    56 MiB) max;     44MB (    42 MiB) avg
Total process virtual memory:   3421MB (  3263 MiB) max;   3403MB (  3246 MiB) avg

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 27) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
