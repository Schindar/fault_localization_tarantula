# 1 "../fault_localization_tarantula/evaluation/bekkouche_benchmark/absminus/AbsMinusKO/AbsMinusKO.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/bekkouche_benchmark/absminus/AbsMinusKO/AbsMinusKO.c"
# 15 "../fault_localization_tarantula/evaluation/bekkouche_benchmark/absminus/AbsMinusKO/AbsMinusKO.c"
extern int __VERIFIER_nondet_int();
extern void __VERIFIER_error();


void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: __VERIFIER_error();
  }
  return;
}


int foo (int i, int j) {
    int result;
    int k = 0;
    if (i <= j) {
        k = k+1;
    }
    if (k == 1 && i != j) {
        result = i-j;
    }
    else {
        result = i-j;
    }
    __VERIFIER_assert( (i<j && result==j-i) || (i>=j && result==i-j));
}


int main()
{

  foo(__VERIFIER_nondet_int(),__VERIFIER_nondet_int());
    return 0;
}
