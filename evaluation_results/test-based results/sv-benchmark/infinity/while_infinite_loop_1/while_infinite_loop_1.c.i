# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/infinity/while_infinite_loop_1/while_infinite_loop_1.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/infinity/while_infinite_loop_1/while_infinite_loop_1.c"
extern void abort(void);
void reach_error(){}

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

int main() {
  int x=0;

  while(1)
  {
    __VERIFIER_assert(x==8);
  }

  __VERIFIER_assert(x!=0);
}
