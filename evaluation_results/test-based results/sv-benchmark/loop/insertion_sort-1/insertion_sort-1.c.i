# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop/insertion_sort-1/insertion_sort-1.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop/insertion_sort-1/insertion_sort-1.c"
extern void abort(void);
void reach_error(){}

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}
unsigned int __VERIFIER_nondet_uint();
int main() {
   unsigned int SIZE=3;
   int i, j, k, key;
   int v[SIZE];
   for (j=1;j<SIZE;j++) {
      key = v[j];
      i = j - 1;
      while((i>=0) && (v[i]>key)) {
         if (i<2)
         v[i+1] = v[i];
         i = i - 1;
      }
      v[i+1] = key;
  }
  for (k=1;k<SIZE;k++)
    __VERIFIER_assert(v[k-1]<=v[k]);
   return 0;
}
