# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop/sum01_bug02/sum01_bug02.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop/sum01_bug02/sum01_bug02.c"
extern void abort(void);
void reach_error(){}

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

extern unsigned int __VERIFIER_nondet_uint();
int main() {
  int i, j=10, n=4, sn=0;
  for(i=1; i<=n; i++) {
    if (i<j)
    sn = n * (2);
    j--;
  }
  __VERIFIER_assert(sn == 0);
}
