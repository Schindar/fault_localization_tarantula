# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop-lit/jm2006/jm2006.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/loop-lit/jm2006/jm2006.c"
extern __VERIFIER_nondet_int();
extern void __VERIFIER_error();

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: __VERIFIER_error();
  }
  return;
}


int main() {
    int i, j;
    i = __VERIFIER_nondet_int();
    j = __VERIFIER_nondet_int();
    if (!(i >= 0 && j >= 0)) return 0;
    int x = i;
    int y = j;
    while(x != 0) {
        x--;
        y = y-3;
    }

    if (i == j) {
        __VERIFIER_assert(y == 0);
    }
    return 0;
}
