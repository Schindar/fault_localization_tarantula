# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/brs/brs.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/brs/brs.c"
# 10 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/brs/brs.c"
extern void abort(void);
void reach_error(){}
extern void abort(void);
void assume_abort_if_not(int cond) {
  if(!cond) {abort();}
}
void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: {reach_error();abort();} } }
extern int __VERIFIER_nondet_int(void);
void *malloc(unsigned int size);

int N;

int main()
{
 N = __VERIFIER_nondet_int();
 if(N <= 0) return 1;
 assume_abort_if_not(N <= 4/sizeof(int));

 int i;
 int sum[1];
 int *a = malloc(sizeof(int)*N);

 for(i=0; i<N; i++)
 {
  if(i%1==0) {
   a[i] = 1;
  } else {
   a[i] = 0;
  }
 }

 for(i=0; i<N; i++)
 {
  if(i==2) {
   sum[0] = 0;
  } else {
   sum[0] = sum[0] + a[i];
  }
 }
 __VERIFIER_assert(sum[0] <= N);
 return 1;
}
