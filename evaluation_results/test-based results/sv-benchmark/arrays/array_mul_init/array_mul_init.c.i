# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/array_mul_init/array_mul_init.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/array_mul_init/array_mul_init.c"
extern void abort(void);
void reach_error(){}
void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: {reach_error();abort();} } }

short __VERIFIER_nondet_short();
int main()
{
 int a[3];
 int b[3];
 int k;
 int i;

 for (i = 0; i<3 ; i++)
 {
  a[i] = i;
  b[i] = i ;
 }

 for (i=0; i< 3; i++)
 {
  if(__VERIFIER_nondet_short())
  {
   k = __VERIFIER_nondet_short();
   a[i] = k;
   b[i] = k / k ;
  }
 }

 for (i=0; i< 3; i++)
 {
  __VERIFIER_assert(a[i] == b[i] || b[i] == a[i] * a[i]);
 }
}
