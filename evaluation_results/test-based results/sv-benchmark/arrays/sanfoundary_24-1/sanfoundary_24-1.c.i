# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/sanfoundary_24-1/sanfoundary_24-1.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "../fault_localization_tarantula/evaluation/sv-benchmark/arrays/sanfoundary_24-1/sanfoundary_24-1.c"
extern void abort(void);
void reach_error(){}
extern int __VERIFIER_nondet_int(void);
void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: {reach_error();abort();} } }







void printEven( int i ) {
  __VERIFIER_assert( ( i % 2 ) == 0 );

}

void printOdd( int i ) {
  __VERIFIER_assert( ( i % 2 ) != 0 );

}

int main()
{
    int array[100000];
    int i;
    int num = __VERIFIER_nondet_int();

  for(i = 0; i < num; i++)
  {
    array[i] = __VERIFIER_nondet_int();
  }


    for (i = 0; i < num; i++)
    {
        if (array[i] % 2 == 1)
        {
            printEven( array[i] );
        }
    }

    for (i = 0; i < num; i++)
    {
        if (array[i] % 2 != 0)
        {
            printOdd( array[i] );
        }
    }
  return 0;
}
