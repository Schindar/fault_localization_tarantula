# Fault localization with Tarantula
This project is in combination with [`TestCov`](https://gitlab.com/sosy-lab/software/test-suite-validator).
## Usage
* Please clone with this command: `git clone --recurse-submodules [url]` since the project includes a submodules.
* First of all you have to generate `test-suites` by [`test-comp`](https://test-comp.sosy-lab.org/2020/) tool.
* Then you have to execute this `test-suites` by the tool [`TestCov`](https://gitlab.com/sosy-lab/software/test-suite-validator) provided by `Thomas lemberger`.
* Please make sure that the folder `test_validator/test-suite-validator/output2/info_files/*.info` is empty in case you want to try another example because there is a loop through the `info_files` and get all `infor_files` from this order and parse into a `data structure`.
* Excution the program with the command: `python Tarantula.py` in the terminal.
* Example: `fault_localization_tarantula % python Tarantula.py`
input_Program`test_false.c` contains the following code:
```
1) extern char __VERIFIER_nondet_char();
2) extern void __VERIFIER_error();
3)
4) int main() {
  5) char a = __VERIFIER_nondet_char();
  6) char b = __VERIFIER_nondet_char();
  7) char c = __VERIFIER_nondet_char();
  8) char d = __VERIFIER_nondet_char();
  9) if (a == 'a' && b == 5 && c == 16 && d == 19) {
  10)  __VERIFIER_error();
 12) }
13) }
14)
15)
16)
```
as output will get this following ranking for each line of the source code in Tarantula_result.txt:

```
3  -->  0.5
5  -->  0.5
6  -->  0.5
7  -->  0.5
8  -->  0.5
9  -->  0.5
10  -->  1.0
11  -->  1.0
12  -->  1.0
15  -->  0.0
16  -->  0.0
```
Expected bugline is 10,11 or 12 since they have the heighst probability.